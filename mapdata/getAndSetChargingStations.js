//Use this file to get mapdata from api.openchargemap.io
//And upload the necessary information it to firebase
const axios = require('axios')
const admin = require('firebase-admin');
const zipCodes = require('./zipCodes.json')
const fs = require('fs')
let serviceAccount
try {
  serviceAccount = require("./serviceAccountKey.json");
  console.log('got serviceAccount Keys')
} catch(e) {
  console.log('Error loading file, get it from https://console.firebase.google.com/project/<YOUR_PROJECT>/settings/serviceaccounts/adminsdk')
}
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://chargely-ca56c.firebaseio.com"
});
const table = 'charging_stations'
const db = admin.firestore()

const deleteCollection = async () => {
  const batch = db.batch()
  const docs = await db.collection(table).listDocuments()
  docs.forEach(val => batch.delete(val))
  await batch.commit()
}

const overWriteDB = async (poiArray) => {
  try {
    let batch = db.batch()

    await deleteCollection()

    for(let i=0; i < poiArray.length; i++ ) {
      const ref = await db.collection(table).doc()
      batch.set(ref, poiArray[i])
    }
    const resp = await batch.commit()
    console.log('resp', resp)


  } catch (e) {
    console.error('overWriteDB', e)
  }

}

// e.g. https://api.openchargemap.io/v4/poi/?output=json&countrycode=US&maxresults=30
const END_POINT = 'https://api.openchargemap.io/v4/poi/?output=json'
const COUNTRY_CODE = '&countrycode='
const MAX_RESULTS = '&maxresults='
const countryResultsEndpoint = (countryCode, maxResults) => `${COUNTRY_CODE}${countryCode}${MAX_RESULTS}${maxResults}`

const api = axios.create({
  baseURL: END_POINT,
  timeout: 60000, // 60 secs
  headers: {
    'Content-Type': 'application/json',
    'Cache-Control': 'no-cache',
    'Accept': 'application/json',
  },
})

const getFormattedName = (operatorInfo, addressInfo) => {
  let name = operatorInfo ? operatorInfo.title : addressInfo.addressLine1;
  const maxLength = 26
  let length = name.length
  let divider = maxLength
  while(length > maxLength){
    const before = name.slice(0, divider)
    const after = name.slice(divider)
    name = before + "\n" + after
    length -= maxLength
    divider += maxLength + 1
  }
  return name
}

const getChargerType = (poi) => {
    connections = poi.connections[0]
    const comment = poi.generalComments ? poi.generalComments.toLowerCase() : ''
    const websiteURL = poi.operatorInfo && poi.operatorInfo.websiteURL ? poi.operatorInfo.websiteURL : ''
    const isTesla = comment.includes('tesla') || comment.includes('super')
                    || websiteURL.includes('tesla')
    const otherConnections = ['J1772', 'Chademo', 'US', 'CCS']
    let connectionType
    if(isTesla) {
        connectionType = 'Tesla'
    } else {
        connectionType = otherConnections[Math.floor(Math.random()*otherConnections.length)];
    }
    // console.log('connectionType', connectionType)
    return connectionType
}

const coalesce = (s, r="") => s ? s : r
// todo get actual hours
const getHours = (poi) => {
  const hours = ["24/7", "8 am - 6 pm", "9 a.m - 5 p.m", "10 a.m - 7 p.m"]
  return hours[Math.floor(Math.random()*hours.length)]
}
const getData = async () => {
  try {
    let poiArray = []
    const { data } = await api.get(countryResultsEndpoint('US', '40000'))
    // console.log('data', data)
    data.forEach(poi => {
      const chargerType = getChargerType(poi)
      const latitude = poi.addressInfo.latitude
      const longitude = poi.addressInfo.longitude
      const zip = poi.addressInfo ? poi.addressInfo.postcode : '98999'
      const isWithinRange = zipCodes['Seattle'][zip] === true || zipCodes['SanFrancisco'][zip] === true
      const fullAddressString = `${poi.addressInfo.addressLine1} ${zip} ${poi.addressInfo.town} ${poi.addressInfo.stateOrProvince}`
      if (isWithinRange) {
        const name = getFormattedName(poi.operatorInfo, poi.addressInfo)
        const doc = {
          name: name, //used
          location: new admin.firestore.GeoPoint(latitude, longitude), //used
          address: coalesce(poi.addressInfo.addressLine1), //used
          fullAddressString: coalesce(fullAddressString), //used
          hours: getHours(poi),
          chargerType: chargerType, //used
          contact: coalesce(poi.addressInfo.contactTelephone1), //used
          placeId: poi.id.toString(), //used
          numberOfPoints: coalesce(poi.numberOfPoints, "1"), //used
          costs: poi.submissionStatus && poi.submissionStatus.usageCosts ? poi.submissionStatus.usageCosts : 'None' ,
          // isMembershipRequired: coalesce(poi.usageType ? .isMembershipRequired),
          comments: coalesce(poi.generalComments),
        }
        poiArray.push(doc)
      }
    })
    console.log('poiArray.length', poiArray.length)
    overWriteDB(poiArray)
  } catch (e) {
    console.error(e)
  }
}
// console.log(getFormattedName("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"))
getData()

