import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_nfc_reader/flutter_nfc_reader.dart';

class NFCPage extends StatefulWidget {
  @override
  _NFCPageState createState() => _NFCPageState();
}

class _NFCPageState extends State<NFCPage> {
  String buttonText = 'Connect to charger via Nfc';
  String subText = '';
  String tagId = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 210),
              _connectNFCButton(),
              Text(subText,
                  style: new TextStyle(
                      fontSize: 18.0, fontWeight: FontWeight.w300))
            ],
          ),
        ),
      ),
    );
  }

  Widget _connectNFCButton() {
    return OutlineButton(
      splashColor: Colors.grey,
      onPressed: () async {
        try {
            setState(() {
              subText = 'No Nfc tag found';
            });
          //activating to make sure NFC is enabled on device
          await FlutterNfcReader.read();
          //consuming scanned tags as a stream
          FlutterNfcReader.onTagDiscovered().listen((onData) {
            String id = onData.id;
            print(id);
            setState(() {
              subText = 'Success, start charging';
            });
            print(onData.content);
          });
        } on PlatformException catch (err) {
          setState(() {
            buttonText = 'Please enable Nfc';
          });
        } catch (error) {
          print("got nfc error");
          print(error);
        }
      },
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
      highlightElevation: 0,
      borderSide: BorderSide(color: Colors.blue),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.nfc, color: Colors.blue),
            Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Text(
                buttonText,
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.grey,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
