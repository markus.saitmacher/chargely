import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import './authentication.dart';
import 'package:flutter_svg/flutter_svg.dart';

enum FormStatus {
  LOGIN_FORM,
  SIGNUP_FORM,
  OTP_FORM,
}

class LoginSignupPage extends StatefulWidget {
  LoginSignupPage({this.auth, this.loginCallback});

  final BaseAuth auth;
  // final VoidCallback loginCallback;
  final void Function(Map<String,String>) loginCallback;

  @override
  State<StatefulWidget> createState() => _LoginSignupPageState();
}

class _LoginSignupPageState extends State<LoginSignupPage> {
  final _formKey = GlobalKey<FormState>();

  String _email;
  String _password;
  String _phoneNumber;
  String _otpCode;
  String _errorMessage;

  FormStatus _formStatus;
  bool _isLoading;
  bool _requestedOTP = false;
  String _otpSource = "email";

  bool isNullEmptyOrFalse(Object o) => o == null || false == o || "" == o;
  bool isNotNullEmptyOrFalse(Object o) => !isNullEmptyOrFalse(o);

  // Check if form is valid before perform login or signup
  // TODO make sure this works with phone number
  bool validateAndSave() {
    print("validating");
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  // Perform login or signup or otp check
  void validateAndSubmit() async {
    try {
      setState(() {
        _errorMessage = "";
        _isLoading = true;
      });
      //TODO make this work
      // if (validateAndSave()) {
      if (true) {
        print("validated form");
        switch (_formStatus) {
          case FormStatus.LOGIN_FORM:
            Map<String, String> userData = await widget.auth.signIn(_email, _password);
            widget.loginCallback(userData);
            break;
          case FormStatus.SIGNUP_FORM:
            //we have to set the state to otp here
            if (isNotNullEmptyOrFalse(_phoneNumber)) {
              await widget.auth.sendOTPToPhone(_phoneNumber);
              _showVerifyCodeDialog("phone");
            } else {
              await widget.auth.sendOTPToEmail(_email);
              _showVerifyCodeDialog("email");
            }
            changeFormModeTo(FormStatus.OTP_FORM);
            break;
          case FormStatus.OTP_FORM:
            Map<String, String> userData;
            print("otp verify");
            if (isNotNullEmptyOrFalse(_phoneNumber)) {
              print("phone verify");
              userData = await widget.auth.signUpPhone(_phoneNumber, _password, _otpCode);
            } else {
              userData = await widget.auth.signUpEmail(_email, _password, _otpCode);
            }
            widget.loginCallback(userData);
            break;
        }
        setState(() {
          _isLoading = false;
        });
      }
    } catch (e) {
      print('Error: $e');
      setState(() {
        _isLoading = false;
        _errorMessage = "Error";
        _formKey.currentState.reset();
      });
    }
  }

  @override
  void initState() {
    // _password="!SuperSecurePa55Wordz72%";
    // _email = "mzj20512@eveav.com";
    _phoneNumber="+1";
    _errorMessage = "";
    _isLoading = false;
    _formStatus = FormStatus.LOGIN_FORM;
    super.initState();
  }

  void resetForm() {
    _formKey.currentState.reset();
    _errorMessage = "";
  }

  void changeFormModeTo(FormStatus formStatus) {
    print(formStatus);
    resetForm();
    setState(() {
      _formStatus = formStatus;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Stack(
          children: <Widget>[
            _showForm(),
            _showCircularProgress(),
          ],
        ));
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

 void _showVerifyCodeDialog(String destination) {
   showDialog(
     context: context,
     builder: (BuildContext context) {
       // return object of type Dialog
       return AlertDialog(
         title:  Text("Verify your account"),
         content:
              Text("A One Time Password to verify your account has been sent to your $destination"),
        //  actions: <Widget>[
        //     FlatButton(
        //      child:  Text("Dismiss"),
        //      onPressed: () {
        //        toggleFormMode();
        //        Navigator.of(context).pop();
        //      },
        //    ),
        //  ],
       );
     },
   );
 }

  Widget _showForm() {
    //TODO make the what to show logic here?
    // if(_formStatus == FormStatus.OTP_FORM) {
    // }
    String primaryButtonText = "";
    String secondaryText = "";
    Widget showPhoneNumber;
    var widgets = <Widget>[];
    switch (_formStatus) {
      case FormStatus.LOGIN_FORM:
        primaryButtonText = "Login";
        secondaryText = "Create an account";
        widgets = <Widget>[
          showLogo(),
          showEmailInput(),
          showPasswordInput(),
          showSpacer(63.0),
          showPrimaryButton(primaryButtonText),
          showSecondaryButton(secondaryText, FormStatus.SIGNUP_FORM),
          showErrorMessage(),
        ];
        break;
      case FormStatus.SIGNUP_FORM:
        primaryButtonText = "Send OTP code";
        secondaryText = "Sign in";
        widgets = <Widget>[
          showLogo(),
          showEmailInput(),
          showText("or"),
          showPhoneNumberInput(),
          showPrimaryButton(primaryButtonText),
          showSecondaryButton(secondaryText, FormStatus.LOGIN_FORM),
        ];
        break;
      case FormStatus.OTP_FORM:
        primaryButtonText = "Create Account";
        secondaryText = "Have an account? Sign in";
        //TODO prepopulate here
        showPhoneNumber = isNotNullEmptyOrFalse(_phoneNumber)
            ? showPhoneNumberInput() : showSpacer(0);
        widgets = <Widget>[
          showLogo(),
          showEmailInput(),
          showPhoneNumber,
          showPasswordInput(),
          showOTPInput(),
          // showSpacer(63.0),
          showPrimaryButton(primaryButtonText),
          showSecondaryButton(secondaryText, FormStatus.LOGIN_FORM),
          showErrorMessage(),
        ];
        break;
    }
    return Container(
        padding: EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: ListView(
            shrinkWrap: true,
            children: widgets,
          ),
        ));
  }

  Widget showErrorMessage() {
    if (isNotNullEmptyOrFalse(_errorMessage)) {
      return Text(
        _errorMessage,
        style: TextStyle(
            fontSize: 13.0,
            color: Colors.red,
            height: 1.0,
            fontWeight: FontWeight.w300),
      );
    } else {
      return Container(
        height: 0.0,
      );
    }
  }

  Widget showLogo() {
    return Hero(
      tag: 'hero',
      child: Padding(
        padding: EdgeInsets.fromLTRB(0.0, 70.0, 0.0, 0.0),
        child: SvgPicture.asset(
          "assets/chronicled_logo_1.svg",
          semanticsLabel: 'Chronicled Logo',
          color: Colors.blue,
          width: 120.0,
        ),
        // child: CircleAvatar(
        //   backgroundColor: Colors.transparent,
        //   radius: 48.0,
        //   // child: Image.asset('assets/flutter-icon.png'),
        //   child: SvgPicture.asset(
        //     "assets/chronicled_logo_1.svg",
        //     semanticsLabel: 'Chronicled Logo',
        //     color: Colors.white,
        //     width: 200.0,
        //   ),
      ),
    );
    // );
  }

  Widget showEmailInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 40.0, 0.0, 0.0),
      child: TextFormField(
        key: Key("email_input"),
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Email',
          icon: Icon(
            Icons.mail,
            color: Colors.grey,
          ),
          // suffixIcon: IconButton(
          //   icon: Icon(
          //     Icons.send,
          //     color: Colors.grey,
          //   ),
          //   onPressed: () {
          //     debugPrint('pressed');
          //   },
        ),
        validator: (value) => isNotNullEmptyOrFalse(value) && isNullEmptyOrFalse(_phoneNumber)
            ? 'Email can\'t be empty'
            : null,
        // onSaved: (value) => _email = value.trim(),
        onChanged: (value) {
          _email = value.trim();
        },
        initialValue: _email,
      ),
    );
  }

  Widget showPasswordInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: TextFormField(
        maxLines: 1,
        obscureText: true,
        autofocus: false,
        decoration: InputDecoration(
            hintText: 'Password',
            icon: Icon(
              Icons.lock,
              color: Colors.grey,
            )),
        validator: (value) => isNullEmptyOrFalse(value) ? 'Password can\'t be empty' : null,
        onChanged: (value) {
          _password = value.trim();
        },
        initialValue: _password,
        // onSaved: (value) => _password = value.trim(),
      ),
    );
  }

  Widget showPhoneNumberInput() {
    var padding = const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0);
    // var padding = _formStatus == FormStatus.SIGNUP_FORM
    //     ? const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0)
    //     : const EdgeInsets.fromLTRB(0.0, 40.0, 0.0, 0.0);
    return Padding(
      padding: padding,
      child: TextFormField(
        key: Key("phone_number_input"),
        maxLines: 1,
        keyboardType: TextInputType.phone,
        autofocus: false,
        decoration: InputDecoration(
            hintText: "Phone number +123456789",
            icon: Icon(
              Icons.local_phone,
              color: Colors.grey,
            )),
        validator: (value) => isNotNullEmptyOrFalse(value) && isNullEmptyOrFalse(_email)
            ? 'Phone number can\'t be empty'
            : null,
        onChanged: (value) {
          _phoneNumber = value.trim();
        },
        initialValue: _phoneNumber,
      ),
    );
  }

  Widget showText(text) {
    return Padding(
        padding: const EdgeInsets.fromLTRB(0.0, 40.0, 0.0, 5.0),
        child: Text(
          text,
          textAlign: TextAlign.center,
        ));
  }

  Widget showOTPInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.number,
        // obscureText: true,
        autofocus: false,
        decoration: InputDecoration(
            //TODO send | sendt otp to phone
            hintText: 'The OTP code that has been send to you',
            icon: Icon(
              Icons.vpn_key,
              color: Colors.grey,
            )),
        //as it is optional
        validator: (value) => isNullEmptyOrFalse(value) ? 'OTP code can\'t be empty' : null,
        // onSaved: (value) => _otpCode = value.trim(),
        onChanged: (value) {
          _otpCode = value.trim();
        },
      ),
    );
  }

  // Widget showSecondaryButtonRow(
  //     {String firstText,
  //     FormStatus firstSwitchDestination,
  //     String secondText,
  //     FormStatus secondSwitchDestination}) {
  //   return Row(
  //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //     children: <Widget>[
  //       FlatButton(
  //           child: Text(firstText,
  //               style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w300)),
  //           onPressed: () => changeFormModeTo(firstSwitchDestination)),
  //       FlatButton(
  //           child: Text(secondText,
  //               style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w300)),
  //           onPressed: () => changeFormModeTo(secondSwitchDestination)),
  //     ],
  //   );
  // }

  Widget showSpacer(double height) {
    return Padding(padding: EdgeInsets.fromLTRB(0.0, height, 0.0, 0.0));
  }

  Widget showSecondaryButton(String text, FormStatus switchDestination) {
    return FlatButton(
      child: Text(text,
          style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w300)),
      onPressed: () => changeFormModeTo(switchDestination),
    );
  }

  Widget showPrimaryButton(String text) {
    return Padding(
        padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
        child: SizedBox(
          height: 40.0,
          child: RaisedButton(
            elevation: 5.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0)),
            color: Colors.blue,
            child: Text(text,
                style: TextStyle(fontSize: 20.0, color: Colors.white)),
            onPressed: validateAndSubmit,
          ),
        ));
  }
}
