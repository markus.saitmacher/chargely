import 'package:flutter/material.dart';
import './authentication.dart';
// import 'dart:async';

class UserPage extends StatefulWidget {
  UserPage(
      {Key key,
      this.auth,
      this.email,
      this.userId,
      this.logoutCallback,
      this.profilePicture})
      : super(key: key);

  final BaseAuth auth;
  final VoidCallback logoutCallback;
  final String userId;
  final String profilePicture;
  final String email;

  @override
  State<StatefulWidget> createState() => new _UserPageState();
}

class _UserPageState extends State<UserPage> {
  @override
  void initState() {
    super.initState();
  }


  signOut() async {
    try {
      await widget.auth.signOut();
      widget.logoutCallback();
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [Colors.blue[100], Colors.blue[400]],
            // colors: [Colors.black[100], Colors.blue[400]],
            // colors: [Colors.pink[400], Colors.red[400]],
          ),
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              CircleAvatar(
                backgroundImage: NetworkImage(
                  widget.profilePicture,
                ),
                radius: 60,
                backgroundColor: Colors.transparent,
              ),
              SizedBox(height: 40),
              Text(
                'Email',
                style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    color: Colors.black54),
              ),
              Text(
                widget.email,
                style: TextStyle(
                    fontSize: 25,
                    color: Colors.blueAccent,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 20),
              Text(
                'Chronicled User ID',
                style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    color: Colors.black54),
              ),
              Text(
                widget.userId,
                style: TextStyle(
                    fontSize: 25,
                    color: Colors.blueAccent,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 20),
              Text(
                'Connected Chargers',
                style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    color: Colors.black54),
              ),
              Text(
                '0',
                style: TextStyle(
                    fontSize: 25,
                    color: Colors.blueAccent,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 40),
              RaisedButton(
                onPressed: () {
                  widget.logoutCallback();
                },
                color: Colors.blueAccent,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Sign Out',
                    style: TextStyle(fontSize: 25, color: Colors.white),
                  ),
                ),
                elevation: 5,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(40)),
              )
            ],
          ),
        ),
      ),
    );
  }
  // @override
  // Widget build(BuildContext context) {
  //   return new Scaffold(
  //       backgroundColor: Colors.white,
  //       appBar: new AppBar(
  //         backgroundColor: Colors.white,
  //         // title: new Text('Flutter login demo'),
  //         actions: <Widget>[
  //           new FlatButton(
  //               child: new Text('Logout',
  //                   style: new TextStyle(fontSize: 17.0, color: Colors.white)),
  //               onPressed: signOut)
  //         ],
  //       ),
  //       body: Text("Logged in"),
  //       // floatingActionButton: FloatingActionButton(
  //       //   onPressed: () {
  //       //     showAddTodoDialog(context);
  //       //   },
  //       //   tooltip: 'Increment',
  //       //   child: Icon(Icons.add),
  //       // )
  //       );
  // }
}
