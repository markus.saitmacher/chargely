import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

abstract class BaseAuth {
  Future<String> sendOTPToEmail(String email);

  Future<String> sendOTPToPhone(String phoneNumber);

  Future<Map<String, String>> signUpPhone(
      String phoneNumber, String password, String otpCode);

  Future<Map<String, String>> signUpEmail(
      String email, String password, String otpCode);

  // Future<FirebaseUser> getCurrentUser();

  // Future<void> sendEmailVerification();

  Future<void> signOut();

  Future<Map<String,String>> signIn(String email, String password);

  // Future<bool> isEmailVerified();
}

//todo: rename to ChronicledAuth
class Auth implements BaseAuth {
  // final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  static String url = 'http://discovery.chronicled.com/api/2.0';
  static Map<String, String> routes = {
    'sendOTPToUser': '/passwordless/start',
    'verifyOTP': '/oauth/ro',
    'user': '/user',
    'signInUser': '/oauth/token'
  };
  static Map<String, String> headers = {
    'Content-Type': 'application/json',
    'Cache-Control': 'no-cache',
    'Accept': 'application/json',
  };

  Future<Map<String,String>> signIn(String email, String password) async {
    Map<String, String> response = await signInUser(email, password);
    String idToken = response["id_token"];
    print(idToken);
    Map<String, dynamic> response2 = await getUserInfo(idToken);
    print("response2");
    print(response2);
    Map<String, String> userData = {
      "idToken": idToken,
      "profilePicture": response2["picture"],
      "userId": response2["user_id"],
      "email": email,
    };
    return userData;
  }

  Future<String> sendOTPToEmail(String email) async {
    Map<String, String> response = await sendOTPToUser("email", email);
    print("response");
    print(response);
    return "success";
  }

  Future<String> sendOTPToPhone(String phoneNumber) async {
    Map<String, String> response = await sendOTPToUser("sms", phoneNumber);
    print("response");
    print(response);
    return "sucess";
  }

  Future<Map<String, String>> signUpEmail(
      String email, String password, String otpCode) async {
    Map<String, String> response = await verifyUserOTP("email", email, otpCode);
    String signUpToken = response["id_token"];
    Map<String, String> response2 =
        await createUser(email, password, signUpToken);
    print("response2");
    print(response2);
    Map<String, String> userData = {
      "idToken": response2["id_token"],
      "profilePicture": response2["picture"],
      "userId": response2["chronicled_id"],
    };
    return userData;
  }

  Future<Map<String, String>> signUpPhone(
      String phoneNumber, String password, String otpCode) async {
    Map<String, String> response =
        await verifyUserOTP("sms", phoneNumber, otpCode);
    String signUpToken = response["id_token"];
    //Ugly workaround for now
    String madeUpEmail = phoneNumber + "@gmail.com";
    Map<String, String> response2 =
        await createUser(madeUpEmail, password, signUpToken);
    Map<String, String> userData = {
      "idToken": response2["id_token"],
      "profilePicture": response2["picture"],
      "userId": response2["chronicled_id"],
      "email": response2["email"]
    };
    print(userData["profilePicture"]);
    return userData;
  }

  Future<Map<String, String>> sendOTPToUser(
      String connection, String destination) async {
    try {
      var method = connection == 'sms' ? 'phone_number' : 'email';

      Map data = {
        "send": "code",
        "connection": connection,
        method: destination,
      };
      var body = json.encode(data);
      final http.Response response = await http
          .post(url + routes['sendOTPToUser'], headers: headers, body: body);
      if (response.statusCode >= 200 ||
          response.statusCode < 400 &&
              (response.body != null && response.body.isNotEmpty)) {
        String source = Utf8Decoder().convert(response.bodyBytes);
        Map<String, String> data = Map();
        data = Map<String, String>.from(json.decode(source));
        return data;
        // return response.body;
      } else {
        throw Exception('Request was not sucessfull, Error:' + response.body);
      }
    } catch (e) {
      print(e);
      //TBD return eror here?
      // return 'unsucessfull';
    }
  }

  Future<Map<String, String>> verifyUserOTP(
      String connection, String destination, String code) async {
    try {
      var method = connection == 'sms' ? 'phone_number' : 'email';

      Map data = {
        "code": code,
        "connection": connection,
        method: destination,
      };
      var body = json.encode(data);
      final http.Response response = await http.post(url + routes['verifyOTP'],
          headers: headers, body: body);
      if (response.statusCode >= 200 ||
          response.statusCode < 400 &&
              (response.body != null && response.body.isNotEmpty)) {
        String source = Utf8Decoder().convert(response.bodyBytes);
        Map<String, String> data = Map();
        data = Map<String, String>.from(json.decode(source));
        return data;
      } else {
        throw Exception('Request was not sucessfull, Error:' + response.body);
      }
    } catch (e) {
      print(e);
      // return 'unsucessfull';
    }
  }

  Future<Map<String, dynamic>> getUserInfo(String jwt) async {
    try {
      headers['Authorization'] = 'Bearer ' + jwt;

      print(url + routes['user']);
      final http.Response response = await http.get(url + routes['user'],
          headers: headers);

      if (response.statusCode >= 200 ||
          response.statusCode < 400 &&
              (response.body != null && response.body.isNotEmpty)) {
        print(response.body);
        String source = Utf8Decoder().convert(response.bodyBytes);
        Map<String, dynamic> data = Map();
        data = Map<String, dynamic>.from(json.decode(source));
        return data;
      } else {
        throw Exception('Request was not sucessfull, Error:' + response.body);
      }
    } catch (e) {
      print(e);
      return null;
    }
  }



  Future<Map<String, String>> signInUser(String email, String password) async {
    try {
      Map data = {
        "username": email,
        "password": password,
      };
      var body = json.encode(data);
      final http.Response response = await http.post(url + routes['signInUser'],
          headers: headers, body: body);
      if (response.statusCode >= 200 ||
          response.statusCode < 400 &&
              (response.body != null && response.body.isNotEmpty)) {
        String source = Utf8Decoder().convert(response.bodyBytes);
        Map<String, String> data = Map();
        data = Map<String, String>.from(json.decode(source));
        return data;
      } else {
        throw Exception('Request was not sucessfull, Error:' + response.body);
      }

    } catch (e) {
      print(e);
    }
  }

  Future<Map<String, String>> createUser(
      String email, String password, String jwt) async {
    try {
      headers['Authorization'] = 'Bearer ' + jwt;

      Map data = {
        "username": email,
        "email": email,
        "password": password,
      };
      var body = json.encode(data);
      final http.Response response = await http.post(url + routes['user'],
          headers: headers, body: body);
      if (response.statusCode >= 200 ||
          response.statusCode < 400 &&
              (response.body != null && response.body.isNotEmpty)) {
        String source = Utf8Decoder().convert(response.bodyBytes);
        Map<String, String> data = Map();
        data = Map<String, String>.from(json.decode(source));
        return data;
      } else {
        throw Exception('Request was not sucessfull, Error:' + response.body);
      }
    } catch (e) {
      print(e);
      // return 'unsucessfull';
    }
  }

  // Future<FirebaseUser> getCurrentUser() async {
  //   FirebaseUser user = await _firebaseAuth.currentUser();
  //   return user;
  // }

  Future<void> signOut() async {
    return () {};
  }

}
