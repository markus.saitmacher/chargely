import 'package:flutter/material.dart';
import './login_signup_page.dart';
import './authentication.dart';
import './user_page.dart';

enum AuthStatus {
  NOT_DETERMINED,
  NOT_LOGGED_IN,
  LOGGED_IN,
}

class RootPage extends StatefulWidget {
  RootPage({this.auth});

  final BaseAuth auth;

  @override
  State<StatefulWidget> createState() => new _RootPageState();
}

class _RootPageState extends State<RootPage> {
  AuthStatus authStatus = AuthStatus.NOT_DETERMINED;
  String _userId = "";
  String _profilePicture = "";
  String _email = "";

  bool isNullEmptyOrFalse(Object o) => o == null || false == o || "" == o;
  bool isNotNullEmptyOrFalse(Object o) => !isNullEmptyOrFalse(o);

  @override
  void initState() {
    super.initState();
    setState(() {
      authStatus = AuthStatus.NOT_LOGGED_IN;
      // authStatus = AuthStatus.LOGGED_IN;
      // _userId="JKAS9a";
    });
    // widget.auth.getCurrentUser().then((user) {
    //   setState(() {
    //     if (user != null) {
    //       _userId = user?.uid;
    //     }
    //     authStatus =
    //         user?.uid == null ? AuthStatus.NOT_LOGGED_IN : AuthStatus.LOGGED_IN;
    //   });
    // });
  }

  void loginCallback(Map<String,String> userData) {
      print("userData");
      print(userData);
      setState(() {
        _userId = userData["userId"];
        _email = userData["email"];
        _profilePicture = userData["profilePicture"];
        authStatus = AuthStatus.LOGGED_IN;
      });
  }

  void logoutCallback() {
    setState(() {
      authStatus = AuthStatus.NOT_LOGGED_IN;
      _userId = "";
      _profilePicture = "";
    });
  }

  Widget buildWaitingScreen() {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        child: CircularProgressIndicator(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    switch (authStatus) {
      case AuthStatus.NOT_DETERMINED:
        return buildWaitingScreen();
        break;
      case AuthStatus.NOT_LOGGED_IN:
        return LoginSignupPage(
          auth: widget.auth,
          loginCallback: loginCallback,
        );
        break;
      case AuthStatus.LOGGED_IN:
        print("logging in");
        print(_userId);
        if (isNotNullEmptyOrFalse(_userId)) {
          print(_userId);
          return UserPage(
            userId: _userId,
            auth: widget.auth,
            email: _email,
            logoutCallback: logoutCallback,
            profilePicture: _profilePicture,
          );
        } else
          return buildWaitingScreen();
        break;
      default:
        return buildWaitingScreen();
    }
  }
}
