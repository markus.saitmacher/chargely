import 'package:flutter/material.dart';
// import 'account/login_page.dart';
import 'account/root_page.dart';
import 'account/authentication.dart';
import 'nfc/nfc_page.dart';
import 'map/google_map.dart';

class Navigation extends StatefulWidget {
  const Navigation({@required this.title});

  final String title;

  @override
  _NavigationState createState() => _NavigationState();
}

class _NavigationState extends State<Navigation> {
  int _currentIndex = 0;
  List<Widget> _children;
  @override
  void initState() {
    _children = [
      StreamBuilderStations(navigateToNFCPage: navigateToNFCPage),
      NFCPage(),
      RootPage(auth: new Auth()),
    ];
    super.initState();
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  void navigateToMapPage(){
    onTabTapped(0);
  }

  void navigateToNFCPage(){
    print("called nfc reroute");
    onTabTapped(1);
  }

  void navigateToAccountPage(){
    onTabTapped(2);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          automaticallyImplyLeading: false,
        title: Text(widget.title),
      ),
      body: IndexedStack(
        index: _currentIndex,
        children: _children,
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.white,
        onTap: onTabTapped,
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(
            icon: new Icon(Icons.map),
            title: new Text('Map'),
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.nfc),
            title: new Text('Connect'),
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.person), title: Text('Account')
          )
        ],
      ),
    );
  }
}
