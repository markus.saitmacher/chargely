// import 'dart:ffi';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:url_launcher/url_launcher.dart';
import 'bottom_styles_theme.dart';
import 'package:flutter/material.dart';

class BottomView extends StatefulWidget {
  BottomView({
    @required this.charger,
    @required this.navigateToNFCPage,
  });

  VoidCallback navigateToNFCPage;
  DocumentSnapshot charger;

  @override
  _BottomViewState createState() => _BottomViewState();
}

class _BottomViewState extends State<BottomView> {
  bool _isFavorited = false;
  void _toggleFavorite() {
    setState(() {
      _isFavorited = !_isFavorited;
    });
  }

  SvgPicture _getPlugImage(String plugType) {
    String assetUrl = 'assets/Charger_US.svg'; //default
    try {
      double width = 25.0;
      switch (plugType) {
        case "CCS":
          assetUrl = 'assets/Charger_CCS.svg';
          break;
        case "Chademo":
          assetUrl = 'assets/Charger_Chademo.svg';
          break;
        case "J1772":
          assetUrl = 'assets/Charger_J1772.svg';
          break;
        case "Tesla":
          assetUrl = 'assets/Charger_Tesla.svg';
          break;
        case "US":
          assetUrl = 'assets/Charger_US.svg';
          break;
      }
      return SvgPicture.asset(
        assetUrl,
        semanticsLabel: assetUrl,
        width: 25.0,
      );
    } catch (e) {
      print("error");
      print(e);
    }
  }

  _rowItems({String key, String value}) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            key,
            textAlign: TextAlign.start,
            style: TextStyle(
              fontFamily: Styles.fontName,
              fontWeight: FontWeight.w800,
              fontSize: 14,
              color: Styles.grey.withOpacity(0.5),
            ),
          ),
          SizedBox(width: 60),
          Padding(
            padding: const EdgeInsets.only(top: 18, right: 10),
            child: Text(
              value,
              textAlign: TextAlign.start,
              style: TextStyle(
                fontFamily: Styles.fontName,
                fontWeight: FontWeight.w800,
                fontSize: 14,
                color: Styles.darkText,
              ),
            ),
          ),
        ]);
  }

  void _launchMap({String address}) async {
    String query = Uri.encodeComponent(address);
    String googleMapsUrl =
        "https://www.google.com/maps/search/?api=1&query=$query";
    String appleMapsUrl = "http://maps.apple.com/?q=$query";
    if (await canLaunch(googleMapsUrl)) {
      await launch(googleMapsUrl);
    } else if (await canLaunch(appleMapsUrl)) {
      await launch(appleMapsUrl);
    } else {
      //not throwing for now
      print('Could not launch url');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      // decoration: BoxDecoration(
      //   color: Styles.white,
      //   borderRadius: BorderRadius.only(
      //       topLeft: Radius.circular(68.0),
      //       bottomLeft: Radius.circular(8.0),
      //       bottomRight: Radius.circular(8.0),
      //       topRight: Radius.circular(68.0)),
      //   boxShadow: <BoxShadow>[
      //     BoxShadow(
      //         color: Styles.grey.withOpacity(0.2),
      //         offset: Offset(1.1, 1.1),
      //         blurRadius: 10.0),
      //   ],
      // ),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 162, right: 162, top: 8),
              child: Container(
                height: 3.5,
                decoration: BoxDecoration(
                  color: Colors.grey.shade300,
                  // color: Styles.background,
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 16, left: 16, right: 24),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(left: 4),
                            child: Text(
                              widget.charger['name'],
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontFamily: Styles.fontName,
                                fontWeight: FontWeight.w600,
                                fontSize: 18,
                                color: Styles.nearlyDarkBlue,
                              ),
                            ),
                          ),
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              IconButton(
                                icon: (_isFavorited
                                    ? Icon(
                                        Icons.favorite,
                                        color: Styles.nearlyDarkBlue,
                                      )
                                    : Icon(Icons.favorite_border)),
                                onPressed: _toggleFavorite,
                              )
                            ],
                          ),
                        ],
                      )
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Icon(Icons.star),
                      Icon(Icons.star),
                      Icon(Icons.star),
                      Icon(Icons.star),
                      Icon(Icons.star),
                    ],
                  )
                ],
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
              child: Container(
                height: 2,
                decoration: BoxDecoration(
                  color: Styles.background,
                  borderRadius: BorderRadius.all(Radius.circular(4.0)),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 24, right: 24, top: 8, bottom: 16),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        _getPlugImage(widget.charger['chargerType']),
                        Padding(
                          padding: const EdgeInsets.only(top: 6),
                          child: Text(
                            'Plug',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontFamily: Styles.fontName,
                              fontWeight: FontWeight.w600,
                              fontSize: 12,
                              color: Styles.grey.withOpacity(0.5),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              '0',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontFamily: Styles.fontName,
                                fontWeight: FontWeight.w500,
                                fontSize: 22,
                                letterSpacing: -0.2,
                                color: Styles.darkText,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 6),
                              child: Text(
                                'Checkins',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontFamily: Styles.fontName,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 12,
                                  color: Styles.grey.withOpacity(0.5),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Text(
                              widget.charger['numberOfPoints'],
                              style: TextStyle(
                                fontFamily: Styles.fontName,
                                fontWeight: FontWeight.w500,
                                fontSize: 22,
                                letterSpacing: -0.2,
                                color: Styles.darkText,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 6),
                              child: Text(
                                "Available chargers",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontFamily: Styles.fontName,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 12,
                                  color: Styles.grey.withOpacity(0.5),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Padding(
                padding: const EdgeInsets.only(
                    left: 4, right: 4, top: 40, bottom: 16),
                // child: Row(
                // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                child: ButtonBar(
                  alignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    OutlineButton(
                      onPressed: () =>
                          _launchMap(address: widget.charger['address']),
                      splashColor: Colors.grey,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40)),
                      highlightElevation: 0,
                      borderSide: BorderSide(color: Styles.nearlyDarkBlue),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(29, 10, 29, 10),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            // Icon(Icons.map, color: Colors.blue),
                            Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: Text(
                                "Navigate",
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.grey,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    OutlineButton(
                      onPressed: widget.navigateToNFCPage,
                      splashColor: Colors.grey,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40)),
                      highlightElevation: 0,
                      borderSide: BorderSide(color: Styles.nearlyDarkBlue),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(29, 10, 29, 10),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            // Icon(Icons.nfc, color: Colors.blue),
                            Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: Text(
                                "Check-in",
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.grey,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                )),
            Padding(
                padding: const EdgeInsets.only(top: 16, right: 24),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 6, left: 24),
                        child: Text(
                          'Access',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontFamily: Styles.fontName,
                            fontWeight: FontWeight.w500,
                            fontSize: 22,
                            letterSpacing: -0.2,
                            color: Styles.darkText,
                          ),
                          // style: TextStyle(
                          //   fontFamily: Styles.fontName,
                          //   fontWeight: FontWeight.w800,
                          //   fontSize: 18,
                          //   color: Styles.grey.withOpacity(0.5),
                          // ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 24, right: 24, top: 8, bottom: 8),
                        child: Container(
                          height: 2,
                          decoration: BoxDecoration(
                            color: Styles.background,
                            borderRadius:
                                BorderRadius.all(Radius.circular(4.0)),
                          ),
                        ),
                      ),
                      Padding(
                          padding: const EdgeInsets.only(
                              left: 24, right: 24, top: 8, bottom: 8),
                          child: Table(children: [
                            TableRow(children: [
                              Padding(
                                padding: const EdgeInsets.only(bottom: 28),
                                child: Text(
                                  "Address",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    fontFamily: Styles.fontName,
                                    fontWeight: FontWeight.w800,
                                    fontSize: 14,
                                    color: Styles.grey.withOpacity(0.5),
                                  ),
                                ),
                              ),
                              Text(
                                widget.charger['fullAddressString'],
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontFamily: Styles.fontName,
                                  fontWeight: FontWeight.w800,
                                  fontSize: 14,
                                  color: Styles.darkText,
                                ),
                              ),
                            ]),
                            TableRow(children: [
                              Padding(
                                padding: const EdgeInsets.only(bottom: 28),
                                child: Text(
                                  "Costs",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    fontFamily: Styles.fontName,
                                    fontWeight: FontWeight.w800,
                                    fontSize: 14,
                                    color: Styles.grey.withOpacity(0.5),
                                  ),
                                ),
                              ),
                              Text(
                                widget.charger['costs'],
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontFamily: Styles.fontName,
                                  fontWeight: FontWeight.w800,
                                  fontSize: 14,
                                  color: Styles.darkText,
                                ),
                              ),
                            ]),
                            TableRow(children: [
                              Padding(
                                padding: const EdgeInsets.only(bottom: 28),
                                child: Text(
                                  "Hours",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    fontFamily: Styles.fontName,
                                    fontWeight: FontWeight.w800,
                                    fontSize: 14,
                                    color: Styles.grey.withOpacity(0.5),
                                  ),
                                ),
                              ),
                              Text(
                                widget.charger['hours'],
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontFamily: Styles.fontName,
                                  fontWeight: FontWeight.w800,
                                  fontSize: 14,
                                  color: Styles.darkText,
                                ),
                              ),
                            ]),
                          ])),
                      // Padding(
                      //     padding: const EdgeInsets.only(left: 18),
                      //     child: _rowItems(key: "Costs", value: widget.charger['costs'],),
                      // ),
                      // Padding(
                      //   padding: const EdgeInsets.only(left: 18),
                      //   child: _rowItems(
                      //     key: "Hours",
                      //     value: '24 / 7',
                      //   ),
                      // ),
                    ])),
          ]),
    );
  }
}
