import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'bottom_view.dart';

class BottomDrawer extends StatefulWidget {
  BottomDrawer({
    @required this.charger,
    @required this.navigateToNFCPage,
  });

  VoidCallback navigateToNFCPage;
  DocumentSnapshot charger;

  @override
  _BottomDrawerState createState() => _BottomDrawerState();
}

class _BottomDrawerState extends State<BottomDrawer>
    with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return DraggableScrollableSheet(
      expand: false,
      initialChildSize: 0.5,
      maxChildSize: 1,
      minChildSize: 0.25,
      builder: (BuildContext context, ScrollController scrollController) {
        return Container(
            color: Colors.white,
            child: ListView.builder(
              controller: scrollController,
              itemCount: 1,
              itemBuilder: (BuildContext context, int index) {
                // return Text("hi");
                return BottomView(charger: widget.charger, navigateToNFCPage: widget.navigateToNFCPage);
              },
            ));
      },
    );
  }
}
