import 'dart:async';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:location/location.dart' as LocationManager;
import 'package:flutter_svg/flutter_svg.dart';
import '../stations_data/bottom_drawer.dart';
import 'package:geolocator/geolocator.dart';

const googleMapsApiKey = 'AIzaSyCzCN9dFqU4whu-QwCdMDUaE4mVG8EX3Iw';
const collectionName = 'charging_stations';
final _placesApiClient = GoogleMapsPlaces(apiKey: googleMapsApiKey);

class StreamBuilderStations extends StatefulWidget {
  StreamBuilderStations({this.navigateToNFCPage});

  VoidCallback navigateToNFCPage;

  @override
  _StreamBuilderStationsState createState() => _StreamBuilderStationsState();
}

class _StreamBuilderStationsState extends State<StreamBuilderStations> {
  Stream<QuerySnapshot> _chargingStations;
  Completer<GoogleMapController> _mapController = Completer();

  @override
  void initState() {
    super.initState();
    _chargingStations = Firestore.instance
        .collection(collectionName)
        .orderBy('name')
        .snapshots();
  }

  @override
  Widget build(BuildContext context) {
    return (StreamBuilder<QuerySnapshot>(
      stream: _chargingStations,
      builder: (context, snapshot) {
        if (snapshot.hasError)
          return Center(child: Text('Error: ${snapshot.error}}'));
        if (!snapshot.hasData) return Center(child: Text('Loading...'));

        return Column(children: [
          Flexible(
            // flex: 2,
            child: StationsMapParent(
              documents: snapshot.data.documents,
              // initialPosition: const LatLng(37.7786, -122.4375), //sf
              initialPosition: const LatLng(47.606209, -122.332069), //seattle
              mapController: _mapController,
              navigateToNFCPage: widget.navigateToNFCPage,
            ),
          ),
        ]);
      },
    ));
  }
}

class StationsMapParent extends StatefulWidget {
  StationsMapParent({
    Key key,
    @required this.documents,
    @required this.initialPosition,
    @required this.mapController,
    @required this.navigateToNFCPage,
  }) : super(key: key);

  VoidCallback navigateToNFCPage;
  final List<DocumentSnapshot> documents;
  final LatLng initialPosition;
  final Completer<GoogleMapController> mapController;

  StationsMap createState() => StationsMap(documents: documents);
}

class StationsMap extends State<StationsMapParent> {
  StationsMap({
    @required this.documents,
  });
  String _selectedMarkerId = "";
  String searchAddr;

  bool _filtering = false;
  List<DocumentSnapshot> documents;

  Future<String> getMapConfigJSON() async {
    return await rootBundle.loadString('assets/map-styling-dark.json');
  }

  void _setMapStyle(GoogleMapController mapController) async {
    String mapStyle = await getMapConfigJSON();
    mapController.setMapStyle(mapStyle);
  }

  void _currentLocation() async {
    LocationManager.Location _location = new LocationManager.Location();
    final GoogleMapController controller = await widget.mapController.future;
    LocationManager.LocationData location;

    try {
      location = await _location.getLocation();
    } on PlatformException catch (e) {
      // print(e.message);
      print(e);
      location = null;
    }

    controller.animateCamera(CameraUpdate.newCameraPosition(
      CameraPosition(
        bearing: 0,
        target: LatLng(location.latitude, location.longitude),
        zoom: 17.0,
      ),
    ));
  }

  void _filterMarkers() async {
    // print('removing docs');
    // // List<DocumentSnapshot> newDocuments = new List<DocumentSnapshot>();
    // if(!_filtered) {
    //   documents.map((document) => {
    //     if (document['placeId'] != '154802') {
    //       document.
    //       document['filtered'] = true;
    //     }
    //     }
    //     );
    // } else {
    //   newDocuments = savedDocuments;
    //   savedDocuments = null;
    // }
    setState(() {
      _filtering = !_filtering;
      // documents = newDocuments;
    });
  }

  searchandNavigate() async {
    try {
      final GoogleMapController mapController =
          await widget.mapController.future;
      List<Placemark> placeMarks =
          await Geolocator().placemarkFromAddress(searchAddr);
      Placemark placeMark = placeMarks[0];
      mapController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
          target:
              LatLng(placeMark.position.latitude, placeMark.position.longitude),
          zoom: 10.0)));
      // } on PlatformException catch (e) {
    } catch (e) {
      print('error');
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    // return GoogleMap(
    return new Scaffold(
        // body: GoogleMap(
        body: Stack(children: <Widget>[
          GoogleMap(
            initialCameraPosition: CameraPosition(
              target: widget.initialPosition,
              zoom: 12,
            ),
            mapToolbarEnabled: false,
            // markers: Set<Marker>.of(markers.values), // YOUR MARKS IN MAP
            markers: documents.map((charger) {
              String id = charger['placeId'];
              if (_filtering && id != '154802') {
                return Marker(markerId: MarkerId(id));
              }
              var color = _selectedMarkerId == id
                  ? BitmapDescriptor.hueBlue
                  : BitmapDescriptor.hueAzure;
              return Marker(
                markerId: MarkerId(id),
                //If else here for document['isChronicledCharger] > blue, pink
                icon: BitmapDescriptor.defaultMarkerWithHue(color),
                position: LatLng(
                  charger['location'].latitude,
                  charger['location'].longitude,
                ),
                onTap: () {
                  // Navigator.of(context).maybePop();
                  // Navigator.pop(context);
                  // widget.navigateToNFCPage();
                  setState(() {
                    _selectedMarkerId = id;
                  });
                  showBottomSheet<void>(
                    context: context,
                    // backgroundColor: Colors.white,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(15.0),
                          topRight: Radius.circular(15.0)),
                    ),
                    builder: (BuildContext context) {
                      return BottomDrawer(
                        navigateToNFCPage: widget.navigateToNFCPage,
                        charger: charger,
                      );
                    },
                  );
                },
              );
            }).toSet(),
            onMapCreated: (mapController) {
              _setMapStyle(mapController);
              try {
                widget.mapController.complete(mapController);
              } catch (e) {
                print('upadate error');
              }
            },
          ),
          Positioned(
            top: 30.0,
            right: 15.0,
            left: 15.0,
            child: Container(
              height: 50.0,
              width: double.infinity,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Colors.white),
              child: TextField(
                decoration: InputDecoration(
                    hintText: 'Enter Address',
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.only(left: 15.0, top: 15.0),
                    suffixIcon: IconButton(
                        icon: Icon(Icons.search),
                        onPressed: searchandNavigate,
                        iconSize: 30.0)),
                onChanged: (val) {
                  setState(() {
                    searchAddr = val;
                  });
                },
              ),
            ),
          )
        ]),
        floatingActionButton: Padding(
          // padding: const EdgeInsets.all(12.0),
          padding: const EdgeInsets.only(top: 105),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              FloatingActionButton(
                heroTag: null,
                // key: Key('filter_list'),
                onPressed: () => _filterMarkers(),
                child: SvgPicture.asset(
                  "assets/chronicled_logo_1.svg",
                  semanticsLabel: 'Chronicled Logo',
                  color: _filtering ? Colors.black : Colors.white,
                  width: 20.0,
                ),
              ),
              FloatingActionButton(
                // key: Key('location_on'),
                heroTag: null,
                onPressed: () => _currentLocation(),
                child: Icon(Icons.location_on),
              )
            ],
          ),
        ));
  }
}
