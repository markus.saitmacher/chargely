import 'package:flutter/material.dart';
import 'navigation.dart';

void main() => runApp(ChargelyApp());


class ChargelyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Chargely',
      home: Navigation(title: 'Chargely'),
      theme: ThemeData(
          canvasColor: Colors.transparent,
          primarySwatch: Colors.blue,
          // scaffoldBackgroundColor: Colors.pink[50],
          bottomSheetTheme:
              BottomSheetThemeData(backgroundColor: Colors.red.withOpacity(0)),
          backgroundColor: Colors.blue
          ),
    );
  }
}
