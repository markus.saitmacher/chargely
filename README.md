# Chargely

This project implements a charging stations map for Electronic Vehicles.
By utilizing the [Chronicled API](https://www.chronicled.com/) ,users are able to unlock chargers by using Nfc tags and the Blockchain.

![screen_shot](./assets/chargely_screenshots_1.jpg)

# How does it work?

![chart](./assets/charegly_diagram.png)

Note: Beforehand the Admin user has to undertake following steps:
- Create Organization via the API
- Create a Spec (to validate blockchain data for things) via the API
- Create a Thing (NFC Plug) via the API
- Configure Event listening and Smart Plug setup over [IFTTT](https://ifttt.com/)


Data for the charging stations is taken from [https://openchargemap.org/site](https://openchargemap.org/site)

# How to run it locally

Run *Flutter in Debug Mode* after setting up Flutter on your machine.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

(Please Note this project currently only compiles to Android, for IOS and web further modifications to the google_maps access have to be taken)
