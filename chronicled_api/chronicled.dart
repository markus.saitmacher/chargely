import 'dart:convert';
import 'package:http/http.dart' as http;

class ChronicledAuth {
  static String url = 'http://discovery.chronicled.com/api/2.0';
  static Map<String, String> routes = {
    'sendOTPToUser': '/passwordless/start',
    'verifyOTP': '/oauth/ro',
    'createUser': '/user',
  };
  static Map<String, String> headers = {
    'Content-Type': 'application/json',
    'Cache-Control': 'no-cache',
    'Accept': 'application/json',
  };

  Future<http.Response> sendOTPToUser(
      String connection, String destination) async {
    try {
      var method = connection == 'sms' ? 'phone_number' : 'email';

      Map data = {
        "send": "code",
        "connection": connection,
        method: destination,
      };
      var body = json.encode(data);
      final http.Response response = await http
          .post(url + routes['sendOTPToUser'], headers: headers, body: body);
      if (response.statusCode >= 200 || response.statusCode < 400) {
        return response;
      } else {
        throw Exception('Request was not sucessfull, Error:' + response.body);
      }
    } catch (e) {
      print(e);
      //TBD return eror here?
      // return 'unsucessfull';
    }
  }

  Future<String> verifyUserOTP(
      String connection, String destination, String code) async {
    try {
      var method = connection == 'sms' ? 'phone_number' : 'email';

      Map data = {
        "code": code,
        "connection": connection,
        method: destination,
      };
      var body = json.encode(data);
      final http.Response response = await http
          .post(url + routes['verifyOTPRoute'], headers: headers, body: body);
      if (response.statusCode >= 200 || response.statusCode < 400) {
        return response.body;
      } else {
        throw Exception('Request was not sucessfull, Error:' + response.body);
      }
    } catch (e) {
      print(e);
      return 'unsucessfull';
    }
  }

  Future<String> createUser(String email, String password, String jwt) async {
    try {
      headers['Authorization'] = 'Bearer ' + jwt;

      Map data = {
        "username": email,
        "email": email,
        "password": password,
      };
      var body = json.encode(data);
      final http.Response response = await http.post(url + routes['createUser'],
          headers: headers, body: body);
      if (response.statusCode >= 200 || response.statusCode < 400) {
        return response.body;
      } else {
        throw Exception('Request was not sucessfull, Error:' + response.body);
      }
    } catch (e) {
      print(e);
      return 'unsucessfull';
    }
  }
}

class ChronicledUser {}

main() async {
  Map<String, String> user3 = {
    'email': 'uth89511@eveav.com',
    'otp': '953765',
    // 'jwt': 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ik9FTTNSVGN3UkRrME56RXlNREpGTUVGRE16UkZORVl6UXpWR01EWkVRVVJGUmtORFJUZzRNUSJ9.eyJyb2xlcyI6WyJndWVzdCJdLCJlbWFpbCI6InV0aDg5NTExQGV2ZWF2LmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJpc3MiOiJodHRwczovL2Nocm9uaWNsZWQtcHJvZC5hdXRoMC5jb20vIiwic3ViIjoiZW1haWx8NWUyM2MxOWQxMTQ4NGQ2NjM0NDA4ZjIxIiwiYXVkIjoieWprdjlnVTBBUDV6UHRGMHhUaXFWTTB4d0xJaWNZRHciLCJpYXQiOjE1Nzk0MDE2NjgsImV4cCI6MTU4MDAwNjQ2OH0.GwfWEH80mzU099u11JMvIBVZMg01iBvJMgzdsz0rok6F0OASfnbgOF8UrVMZPezuzZtgWVu3MyMrDy7jQ_58WwHFDg0nmMeljiuKgXIPT-R9YzKYTH1cy6iy8VgWm89OAXgBAQhq4u25ur3Z95dZGoligvr9B-5vLqB0rbbgT62bw9YpYjjASd40qBPZJcer3FSSv77OOiNOPSV3YqxXMgtE8T_l8yJbUpgR74r5QKTFmeXY5PItUqyrNSnW2WBpHMvBUhvv4qFG4wlcLR0wUT3YbJGDqV-ibX1YMupCJDr4GggNZPRXfspudrOZ2Eb6F1vSecjGcEiGD6IM_Hb0yw',
    'jwt':
        'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ik9FTTNSVGN3UkRrME56RXlNREpGTUVGRE16UkZORVl6UXpWR01EWkVRVVJGUmtORFJUZzRNUSJ9.eyJyb2xlcyI6WyJ1c2VyIl0sImNocm9uaWNsZWRfaWQiOiJkU3RkZXdSIiwiZW1haWwiOiJ1dGg4OTUxMUBldmVhdi5jb20iLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImlzcyI6Imh0dHBzOi8vY2hyb25pY2xlZC1wcm9kLmF1dGgwLmNvbS8iLCJzdWIiOiJhdXRoMHw1ZTIzYzU5ZjZjNmI0MzBmMTNkODBkNTUiLCJhdWQiOiJ5amt2OWdVMEFQNXpQdEYweFRpcVZNMHh3TElpY1lEdyIsImlhdCI6MTU3OTQwMjY1NSwiZXhwIjoxNTgwMDA3NDU1fQ.B_E5MsnFKzp7vTujH4N43SURBwnSpFVNJehJtscHb6Xsv5XbqqBVaZC0B8soQeNTrExmF0383djqIKC9M7DeXTcLMClbElWjRlifuvVTtCaydlxnudSgVErxnSEwQB4Sq7ZISJ_iomxunTsrZXsDEbx2L5zU9BoMyrOsT0vjsasSmY7mOpcTR92A-zA578nwLBawh5dMqxJW9gIEwPf-xoWmvQ4ILJeYVijJ_BANKe6WHJ4rldOrEYguob7Z-uZn2be1-Ax2erpHxr0aHYL0ue187DMTvMrBNjE6C3-Q2T1OA1KJErF614AySrYGmn-IOmYt5T7DvenuXvpZQZ_Q9g',
    'password': '!Hallo12345',
  };
  ChronicledAuth c = ChronicledAuth();
  http.Response response = await c.sendOTPToUser("email", user3['email']);
  if (response.body != null && response.body.isNotEmpty) {
    String source = Utf8Decoder().convert(response.bodyBytes);
    Map<String, String> data = Map();
    data = Map<String, String>.from(json.decode(source));
  }

  // String response = await c.sendOTPToUser("sms", "+12066699681");

  String response2 = await c.verifyUserOTP("email", user3['email'], user3['otp']);
  print(response2);
  //get jwt from response and create user from it
  // String response = await c.createUser(user3['email'], user3['password'], user3['jwt']);
  // Get avatar here and display it!
  // print(response);
}
